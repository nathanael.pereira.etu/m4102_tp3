package fr.ulille.iut.pizzaland.beans;

import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzasDto;

public class Pizzas {

	private long id;
	private String name;
	
	public Pizzas() {
	}
	
	public Pizzas(long id, String name) {
		this.id = id;
		this.name =name;
	}
	
	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public static Pizzas fromDto(PizzasDto dto) {
		Pizzas pizzas = new Pizzas();
		pizzas.setId(dto.getId());
		pizzas.setName(dto.getName());

		return pizzas;
	}
}
