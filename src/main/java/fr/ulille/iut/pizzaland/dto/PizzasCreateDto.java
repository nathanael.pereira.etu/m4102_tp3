package fr.ulille.iut.pizzaland.dto;

public class PizzasCreateDto {
private String name;
	
	public PizzasCreateDto() {}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

}
